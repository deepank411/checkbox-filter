var myApp = angular.module('myApp',[]);

var uniqueItems = function (data, key) {
   var result = [];

   for (var i = 0; i < data.length; i++) {
      var value = data[i][key];

      if (result.indexOf(value) == -1) {
         result.push(value);
      }

   }
   return result;
};

myApp.controller('MyCtrl',['$scope', 'filterFilter',
function($scope, filterFilter) {
   $scope.useLocations = {};
   $scope.useSeasons = {};
   $scope.useDurations = {};
   $scope.useTags = {};

   $scope.places = [
      {
         name: "The Taj Mahal",
         location: "India",
         season: "Anytime",
         duration: "less than 2 days",
         tag: "Historical"
      },
      {
         name: "The Bhangarh Fort",
         location: "India",
         season: "January-March",
         duration: "2 Days",
         tag: "Haunted"
      },
      {
         name: "The Gateway Of India",
         location: "India",
         season: "Anytime",
         duration: "5 Days",
         tag: "Weekend Getaways"
      },
      {
         name: "The Lotus Temple",
         location: "USA",
         season: "Anytime",
         duration: "5 Days",
         tag: "Religious"
      }
   ];

      // Watch the pants that are selected
      $scope.$watch(function () {
         return {
            places: $scope.places,
            useLocations: $scope.useLocations,
            useSeasons: $scope.useSeasons,
            useDurations: $scope.useDurations,
            useTags: $scope.useTags
         }
      }, function (value) {
         var selected;

         // $scope.count = function (prop, value) {
         //    return function (el) {
         //       return el[prop] == value;
         //    };
         // };

         $scope.locationsGroup = uniqueItems($scope.places, 'location');
         var filterAfterLocations = [];
         selected = false;
         for (var j in $scope.places) {
            var p = $scope.places[j];
            for (var i in $scope.useLocations) {
               if ($scope.useLocations[i]) {
                  selected = true;
                  if (i == p.location) {
                     filterAfterLocations.push(p);
                     break;
                  }
               }
            }
         }
         if (!selected) {
            filterAfterLocations = $scope.places;
         }

         $scope.seasonsGroup = uniqueItems($scope.places, 'season');
         var filterAfterSeasons = [];
         selected = false;
         for (var j in filterAfterLocations) {
            var p = filterAfterLocations[j];
            for (var i in $scope.useSeasons) {
               if ($scope.useSeasons[i]) {
                  selected = true;
                  if (i == p.season) {
                     filterAfterSeasons.push(p);
                     break;
                  }
               }
            }
         }
         if (!selected) {
            filterAfterSeasons = filterAfterLocations;
         }

         $scope.tagsGroup = uniqueItems($scope.places, 'tag');
         var filterAfterTags = [];
         selected = false;
         for (var j in filterAfterSeasons) {
            var p = filterAfterSeasons[j];
            for (var i in $scope.useTags) {
               if ($scope.useTags[i]) {
                  selected = true;
                  if (i == p.tag) {
                     filterAfterTags.push(p);
                     break;
                  }
               }
            }
         }
         if (!selected) {
            filterAfterTags = filterAfterSeasons;
         }

         $scope.durationsGroup = uniqueItems($scope.places, 'duration');
         var filterAfterDurations = [];
         selected = false;
         for (var j in filterAfterTags) {
            var p = filterAfterTags[j];
            for (var i in $scope.useDurations) {
               if ($scope.useDurations[i]) {
                  selected = true;
                  if (i == p.duration) {
                     filterAfterDurations.push(p);
                     break;
                  }
               }
            }
         }
         if (!selected) {
            filterAfterDurations = filterAfterTags;
         }

         $scope.filteredPlaces = filterAfterDurations;
      }, true);


      $scope.$watch('filtered', function (newValue) {
         if (angular.isArray(newValue)) {
            console.log(newValue.length);
         }
      }, true);
   }]);

   myApp.filter('count', function() {
      return function(collection, key) {
         var out = "test";
         for (var i = 0; i < collection.length; i++) {
            //console.log(collection[i].pants);
            //var out = myApp.filter('filter')(collection[i].pants, "42", true);
         }
         return out;
      }
   });


   myApp.filter('groupBy',
   function () {
      return function (collection, key) {
         if (collection === null) return;
         return uniqueItems(collection, key);
      };
   });
